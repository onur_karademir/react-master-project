import React from "react";
import "./App.css";
import { Button, Alert } from "reactstrap";

// import { v4 as uuidv4 } from 'uuid'; /// uuid import exemple //

//if you want use axios -->>> import_npm_list.txt inside axios you can learn from web page//

function App() {
  return (
    /// reactstrap and bootstrap exemple start container///
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <Alert color="primary">This is a primary alert — check it out!</Alert>
          <Button>click</Button>
        </div>
      </div>
    </div>
  );
}

export default App;
